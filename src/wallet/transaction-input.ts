export default class TransactionInput {
    readonly amount: number;
    readonly address: string;
    readonly timestamp: number;
    readonly signature: string;

    constructor(amount: number, address: string, timestamp: number, signature: string) {
        this.amount = amount;
        this.address = address;
        this.timestamp = timestamp;
        this.signature = signature;
    }
}