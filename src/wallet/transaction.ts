import TransactionInput from "./transaction-input";
import TransactionOutput from "./transaction-output";

export default class Transaction {
    readonly id: string;
    readonly txInput: TransactionInput;
    readonly txOutput: TransactionOutput[];

    constructor(id: string, txInput: TransactionInput, txOutput: TransactionOutput[]) {
        this.id = id;
        this.txInput = txInput;
        this.txOutput = txOutput;
    }
}
