import Block from "./block";

export default class Blockchain {
    readonly chain: Block[];

    constructor(chain: Block[] = [Block.getGenesisBlock()]) {
        this.chain = chain;
    }
}