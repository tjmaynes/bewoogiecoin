import Transaction from "../wallet/transaction";
import TransactionInput from "../wallet/transaction-input";

export default class Block {
    readonly timestamp: number;
    readonly lastHash: string;
    readonly hash: string;
    readonly data: Transaction;
    readonly nonce: number;
    readonly difficulty: number;
    
    constructor(timestamp: number, lastHash: string, hash: string, data: any, nonce:number, difficulty: number = 3) {
        this.timestamp = timestamp;
        this.lastHash = lastHash;
        this.hash = hash;
        this.data = data;
        this.nonce = nonce;
        this.difficulty = difficulty;
    }

    static getGenesisBlock(difficulty: number = 5): Block {
        const txInput = new TransactionInput(0, "-----", 0, "");
        const genesisTx = new Transaction("genesis", txInput, []);
        return new Block(0, '-----', 'some-hash', genesisTx, 0, difficulty);
    }
};
