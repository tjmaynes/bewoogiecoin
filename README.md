# BewoogieCoin
> Toy blockchain and cryptocurrency

## Requirements

- [Nodejs](https://nodejs.org/en/)

## Usage

To install project dependencies, run the following command:
```bash
make install
```

To start the project, run the following command:
```bash
make start
```

To run all tests, run the following command:
```bash
make test
```