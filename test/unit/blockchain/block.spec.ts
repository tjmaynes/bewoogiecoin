import Block from "../../../src/blockchain/block";

describe("Block", () => {
    describe("#getGenesisBlock", () => {
        it("should return new genesis block", () => {
            const difficulty = 2;

            const genesisBlock = Block.getGenesisBlock(difficulty);
            
            expect(genesisBlock.timestamp).toEqual(0);
            expect(genesisBlock.difficulty).toEqual(difficulty);
            expect(genesisBlock.lastHash).toEqual("-----");
            expect(genesisBlock.hash).toEqual("some-hash");
            expect(genesisBlock.nonce).toEqual(0);
            expect(genesisBlock.data.id).toEqual("genesis");
            expect(genesisBlock.data.txInput.timestamp).toEqual(0);
        });
    });
});