install:
	npm install

start:
	npm start

.PHONY: test
test:
	npm test

deploy: test
