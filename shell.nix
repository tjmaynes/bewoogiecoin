with import <nixpkgs> {};

stdenv.mkDerivation {
  name = "bewoogiecoin";
  buildInputs = [
    nodejs-16_x
  ];
  shellHook = ''
    export PATH=$PWD/node_modules/.bin:$PATH
    make install
  '';
}
